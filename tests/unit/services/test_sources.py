import hashlib

import pypeg2
import pytest
from duniterpy.documents.transaction import output

from sakia.data.entities import Transaction
from sakia.data.repositories import TransactionsRepo
from sakia.services import sources


def test_parse_source_condition(application_with_one_connection, alice, bob):
    application_with_one_connection.instanciate_services()

    issuer = alice.key.pubkey
    receiver = bob.key.pubkey
    condition = output.Condition.token(
        output.SIG.token(receiver),
        output.Operator.token("||"),
        output.Condition.token(
            output.SIG.token(issuer),
            output.Operator.token("&&"),
            output.CSV.token(604800),
        ),
    )
    assert (
        application_with_one_connection.sources_service.find_signature_in_condition(
            condition, receiver
        )
        is True
    )
    assert (
        application_with_one_connection.sources_service.find_signature_in_condition(
            condition, issuer
        )
        is True
    )
    assert (
        application_with_one_connection.sources_service.find_signature_in_condition(
            condition, "badpubkey"
        )
        is False
    )
    condition = output.Condition.token(
        output.Condition.token(
            output.CSV.token(604800),
            output.Operator.token("&&"),
            output.SIG.token(issuer),
        ),
        output.Operator.token("||"),
        output.SIG.token(receiver),
    )

    assert (
        application_with_one_connection.sources_service.find_signature_in_condition(
            condition, receiver
        )
        is True
    )
    assert (
        application_with_one_connection.sources_service.find_signature_in_condition(
            condition, issuer
        )
        is True
    )
    assert (
        application_with_one_connection.sources_service.find_signature_in_condition(
            condition, "badpubkey"
        )
        is False
    )


def test_evaluate_condition_source_lock_mode_0(
    application_with_one_connection, bob, alice, meta_repo
):
    application_with_one_connection.instanciate_services()

    # capture blockchain median time
    median_time = (
        application_with_one_connection.blockchain_service._blockchain_processor.time(
            application_with_one_connection.currency
        )
    )
    tx_hash = "FCAD5A388AC8A811B45A9334A375585E77071AA9F6E5B6896582961A6C66F365"

    # add a transaction in bob tx history
    transactions_repo = TransactionsRepo(meta_repo.conn)
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            bob.key.pubkey,
            tx_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,
            bob.key.pubkey,
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            alice.key.pubkey,
            tx_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,
            bob.key.pubkey,
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    # test simple signature condition
    condition = output.Condition.token(
        output.SIG.token(bob.key.pubkey),
    )
    # bob can spend this source
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        condition,
        [bob.key.pubkey],
        [],
        tx_hash,
    )
    assert result is True
    assert _errors is None

    # alice can not
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        condition,
        [alice.key.pubkey],
        [],
        tx_hash,
    )
    assert result is False
    assert _errors == [
        (
            pypeg2.compose(condition),
            sources.EVALUATE_CONDITION_ERROR_SIG,
            bob.key.pubkey,
        )
    ]


def test_evaluate_condition_source_lock_mode_1(
    application_with_one_connection, bob, alice, meta_repo
):
    application_with_one_connection.instanciate_services()

    # capture blockchain median time
    median_time = (
        application_with_one_connection.blockchain_service._blockchain_processor.time(
            application_with_one_connection.currency
        )
    )
    tx_hash = "FCAD5A388AC8A811B45A9334A375585E77071AA9F6E5B6896582961A6C66F365"

    # add a transaction in bob tx history
    transactions_repo = TransactionsRepo(meta_repo.conn)
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            bob.key.pubkey,
            tx_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,
            bob.key.pubkey,
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            alice.key.pubkey,
            tx_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,
            bob.key.pubkey,
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )

    # test condition: receiver or (issuer and CSV(one week))
    delay = 604800
    condition = output.Condition.token(
        output.SIG.token(bob.key.pubkey),
        output.Operator.token("||"),
        output.Condition.token(
            output.SIG.token(alice.key.pubkey),
            output.Operator.token("&&"),
            output.CSV.token(delay),
        ),
    )
    # bob spend his source
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        condition,
        [bob.key.pubkey],
        [],
        tx_hash,
    )
    assert result is True
    assert _errors == [
        (
            "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo",
        ),
        ("CSV(604800)", sources.EVALUATE_CONDITION_ERROR_CSV, median_time + delay),
    ]

    # alice try to get back this source before the CSV delay
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        condition,
        [alice.key.pubkey],
        [],
        tx_hash,
    )
    assert result is False
    assert _errors == [
        ("CSV(604800)", sources.EVALUATE_CONDITION_ERROR_CSV, median_time + delay),
        (
            "SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7",
        ),
    ]

    # Transaction was made one week ago...
    transactions_repo.update(
        Transaction(
            "testcurrency",
            bob.key.pubkey,
            tx_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time - delay,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,
            bob.key.pubkey,
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    transactions_repo.update(
        Transaction(
            "testcurrency",
            alice.key.pubkey,
            tx_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time - delay,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,
            bob.key.pubkey,
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    tx = transactions_repo.get_one(sha_hash=tx_hash)
    assert tx.timestamp == median_time - delay

    # bob try to spend his source
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        condition,
        [bob.key.pubkey],
        [],
        tx_hash,
    )
    assert result is True
    assert _errors == [
        (
            "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo",
        )
    ]

    # alice can get back this source after the CSV delay
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        condition,
        [alice.key.pubkey],
        [],
        tx_hash,
    )
    assert result is True
    assert _errors == [
        (
            "SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7",
        )
    ]


def test_evaluate_condition_source_multisig(
    application_with_one_connection, bob, alice, meta_repo
):
    application_with_one_connection.instanciate_services()

    # capture blockchain median time
    median_time = (
        application_with_one_connection.blockchain_service._blockchain_processor.time(
            application_with_one_connection.currency
        )
    )
    tx_hash = "FCAD5A388AC8A811B45A9334A375585E77071AA9F6E5B6896582961A6C66F365"

    # add a transaction in bob tx history
    transactions_repo = TransactionsRepo(meta_repo.conn)
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            bob.key.pubkey,
            tx_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,
            bob.key.pubkey,
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            alice.key.pubkey,
            tx_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,
            bob.key.pubkey,
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )

    # test condition: multi signatures
    condition = output.Condition.token(
        output.SIG.token(bob.key.pubkey),
        output.Operator.token("&&"),
        output.SIG.token(alice.key.pubkey),
    )
    # bob can not spend this source alone
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        condition,
        [bob.key.pubkey],
        [],
        tx_hash,
    )
    assert result is False
    assert _errors == [
        (
            "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo",
        )
    ]

    # alice can not spend this source alone
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        condition,
        [alice.key.pubkey],
        [],
        tx_hash,
    )
    assert result is False
    assert _errors == [
        (
            "SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7",
        )
    ]

    # alice && bob together only can spend this source
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        condition,
        [alice.key.pubkey, bob.key.pubkey],
        [],
        tx_hash,
    )
    assert result is True
    assert _errors is None


def test_evaluate_condition_source_atomic_swap(
    application_with_one_connection, bob, alice, meta_repo
):
    application_with_one_connection.instanciate_services()
    transactions_repo = TransactionsRepo(meta_repo.conn)

    # capture blockchain median time
    median_time = (
        application_with_one_connection.blockchain_service._blockchain_processor.time(
            application_with_one_connection.currency
        )
    )

    tx1_hash = "FCAD5A388AC8A811B45A9334A375585E77071AA9F6E5B6896582961A6C66F365"
    # add TX1 transaction in alice tx history
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            alice.key.pubkey,  # alice history
            tx1_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,  # do not care
            bob.key.pubkey,  # do not care
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    # add TX1 transaction in bob tx history
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            bob.key.pubkey,  # bob history
            tx1_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,  # do not care
            bob.key.pubkey,  # do not care
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )

    # atomic swap initiator TX1 condition created by alice
    # (XHX(password) && SIG(bob)) || (SIG(alice) && SIG(bob)) || (SIG(alice) && CSV(48h))
    password = "test".encode("utf8")  # password must be encoded in str not unicode
    hash_password = hashlib.sha256(password).hexdigest().upper()
    delay_48h = 172800
    tx1_condition = output.Condition.token(
        output.Condition.token(
            output.Condition.token(
                output.XHX.token(hash_password),
                output.Operator.token("&&"),
                output.SIG.token(bob.key.pubkey),
            ),
            output.Operator.token("||"),
            output.Condition.token(
                output.SIG.token(alice.key.pubkey),
                output.Operator.token("&&"),
                output.SIG.token(bob.key.pubkey),
            ),
        ),
        output.Operator.token("||"),
        output.Condition.token(
            output.SIG.token(alice.key.pubkey),
            output.Operator.token("&&"),
            output.CSV.token(delay_48h),
        ),
    )

    tx2_hash = "ACAB5A388AC8A811B45A9334A375585E77071AA9F6E5B6896582961A6C66F365"
    # add TX2 transaction in alice tx history
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            alice.key.pubkey,  # alice history
            tx2_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,  # do not care
            bob.key.pubkey,  # do not care
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    # add TX2 transaction in bob tx history
    transactions_repo.insert(
        Transaction(
            "testcurrency",
            bob.key.pubkey,  # bob history
            tx2_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,  # do not care
            bob.key.pubkey,  # do not care
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )

    # test atomic swap participant TX2 condition created by bob with alice password hash
    # (XHX(password) && SIG(alice)) || (SIG(alice) && SIG(bob)) || (SIG(bob) && CSV(24h))
    delay_24h = 86400
    tx2_condition = output.Condition.token(
        output.Condition.token(
            output.Condition.token(
                output.XHX.token(hash_password),
                output.Operator.token("&&"),
                output.SIG.token(alice.key.pubkey),
            ),
            output.Operator.token("||"),
            output.Condition.token(
                output.SIG.token(alice.key.pubkey),
                output.Operator.token("&&"),
                output.SIG.token(bob.key.pubkey),
            ),
        ),
        output.Operator.token("||"),
        output.Condition.token(
            output.SIG.token(bob.key.pubkey),
            output.Operator.token("&&"),
            output.CSV.token(delay_24h),
        ),
    )

    # alice spend the source from tx2 with the password
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        tx2_condition,
        [alice.key.pubkey],
        [password],
        tx2_hash,
    )
    assert result is True
    assert _errors == [
        (
            "SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7",
        ),
        (
            "SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7",
        ),
        ("CSV(86400)", sources.EVALUATE_CONDITION_ERROR_CSV, median_time + delay_24h),
    ]

    # the password is revealed in the unlock of the tx3 spending tx2
    # bob can now spend tx1 using the password
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        tx1_condition,
        [bob.key.pubkey],
        [password],
        tx1_hash,
    )
    assert result is True
    assert _errors == [
        (
            "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo",
        ),
        (
            "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo",
        ),
        ("CSV(172800)", "locked by a delay until", median_time + delay_48h),
    ]

    # alice and bob can sign together to spend tx1
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        tx1_condition,
        [bob.key.pubkey, alice.key.pubkey],
        [password],
        tx1_hash,
    )
    assert result is True
    assert _errors == [
        ("CSV(172800)", sources.EVALUATE_CONDITION_ERROR_CSV, median_time + delay_48h)
    ]

    # alice and bob can sign together to spend tx2
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        tx2_condition,
        [bob.key.pubkey, alice.key.pubkey],
        [password],
        tx2_hash,
    )
    assert result is True
    assert _errors == [
        ("CSV(86400)", sources.EVALUATE_CONDITION_ERROR_CSV, median_time + delay_24h)
    ]

    # alice can not spend the source from tx2 without the password
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        tx2_condition,
        [alice.key.pubkey],
        [],
        tx2_hash,
    )
    assert result is False
    assert _errors == [
        (
            "XHX(9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08)",
            sources.EVALUATE_CONDITION_ERROR_XHX,
            "9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08",
        ),
        (
            "SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7",
        ),
        (
            "SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7",
        ),
        ("CSV(86400)", "locked by a delay until", median_time + delay_24h),
    ]

    # bob can not spend tx1 without the password
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        tx1_condition,
        [bob.key.pubkey],
        [],
        tx1_hash,
    )
    assert result is False
    assert _errors == [
        (
            "XHX(9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08)",
            sources.EVALUATE_CONDITION_ERROR_XHX,
            "9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08",
        ),
        (
            "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo",
        ),
        (
            "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo",
        ),
        ("CSV(172800)", sources.EVALUATE_CONDITION_ERROR_CSV, median_time + delay_48h),
    ]

    # TX1 Transaction was made 48h ago...
    # update TX1 transaction in alice tx history
    transactions_repo.update(
        Transaction(
            "testcurrency",
            alice.key.pubkey,  # alice history
            tx1_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time - delay_48h,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,  # do not care
            bob.key.pubkey,  # do not care
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    # update TX1 transaction in bob tx history
    transactions_repo.update(
        Transaction(
            "testcurrency",
            bob.key.pubkey,  # bob history
            tx1_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time - delay_48h,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,  # do not care
            bob.key.pubkey,  # do not care
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )

    # TX2 Transaction was made 24h ago...
    # update TX2 transaction in alice tx history
    transactions_repo.update(
        Transaction(
            "testcurrency",
            alice.key.pubkey,  # alice history
            tx2_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time - delay_24h,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,  # do not care
            bob.key.pubkey,  # do not care
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )
    # update TX2 transaction in bob tx history
    transactions_repo.update(
        Transaction(
            "testcurrency",
            bob.key.pubkey,  # bob history
            tx2_hash,
            20,
            "15-76543400E78B56CC21FB1DDC6CBAB24E0FACC9A798F5ED8736EA007F38617D67",
            median_time - delay_24h,
            "H41/8OGV2W4CLKbE35kk5t1HJQsb3jEM0/QGLUf80CwJvGZf3HvVCcNtHPUFoUBKEDQO9mPK3KJkqOoxHpqHCw==",
            alice.key.pubkey,  # do not care
            bob.key.pubkey,  # do not care
            1565,
            1,
            "",
            0,
            Transaction.VALIDATED,
        )
    )

    # alice can get back the source from tx1 without the password after 48h
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        tx1_condition,
        [alice.key.pubkey],
        [],
        tx1_hash,
    )
    assert result is True
    assert _errors == [
        (
            "XHX(9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08)",
            sources.EVALUATE_CONDITION_ERROR_XHX,
            "9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08",
        ),
        (
            "SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7",
        ),
        (
            "SIG(GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "GfFUvqaVSgCt6nFDQCAuULWk6K16MUDckeyBJQFcaYj7",
        ),
    ]

    # bob can spend tx2 without the password after 24h
    (
        result,
        _errors,
    ) = application_with_one_connection.sources_service.evaluate_condition(
        application_with_one_connection.currency,
        tx2_condition,
        [bob.key.pubkey],
        [],
        tx2_hash,
    )
    assert result is True
    assert _errors == [
        (
            "XHX(9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08)",
            sources.EVALUATE_CONDITION_ERROR_XHX,
            "9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08",
        ),
        (
            "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo",
        ),
        (
            "SIG(F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo)",
            sources.EVALUATE_CONDITION_ERROR_SIG,
            "F3HWkYnUSbdpEueosKqzYd1m8ftwojwE2uXR7ScoAVKo",
        ),
    ]
