import json

from sakia.data.entities.transaction import Transaction, parse_transaction_doc
from duniterpy.documents.transaction import Transaction as TransactionDoc
from duniterpy.documents.transaction import reduce_base


def test_parse_transaction_doc():

    bma_response = json.loads(
        """{
        "version": 10,
        "locktime": 0,
        "blockstamp": "310818-000002B26A7B623915E4955C633FB710D90758AA1D778B2D557518EA55B1F5FE",
        "blockstampTime": 1585901723,
        "issuers": [
          "EAHgNyYsxmS7YSfuQsEzEWwKnD2UyMzwVTuBnstMSk3e"
        ],
        "inputs": [
          "1016:0:D:EAHgNyYsxmS7YSfuQsEzEWwKnD2UyMzwVTuBnstMSk3e:310568",
          "120:0:T:26DC6DD90DB2C7EE6068F307DB95D5E41DA8C56E95843FD4976F70A303D3BBF0:27",
          "30060:0:T:3A1AABA9977CBCE4E25E0F0BA49539BE21AE94048C1DCDE72560BF89D5FAEFBF:1"
        ],
        "outputs": [
          "20000:0:SIG(7F6oyFQywURCACWZZGtG97Girh9EL1kg2WBwftEZxDoJ)",
          "11196:0:SIG(EAHgNyYsxmS7YSfuQsEzEWwKnD2UyMzwVTuBnstMSk3e)"
        ],
        "unlocks": [
          "0:SIG(0)",
          "1:SIG(0)",
          "2:SIG(0)"
        ],
        "signatures": [
          "FPimg3/Xmb/qRIGP3ZjwV8yH/cbqb6AIgZ49lnwcQ2S9cIj1BDCdhKpDKfm3nFkLoGZUZXXkmGXSoTehtx5JDQ=="
        ],
        "comment": "pour le ssb de scuttlebutt dans duniterpy",
        "hash": "50976B6D16DAC4FE8D255CCA9EBA9D9E508C23EE824124DF988BA9B76A4DCC98",
        "time": 1585902265,
        "block_number": 310820,
        "received": null
      }
        """
    )
    txdoc = TransactionDoc.from_bma_history("testcurrency", bma_response)

    # fixme: from_bma_history() return a bad hash (from signed raw)... So why there is a good hash (without signatures) in db ?
    # assert txdoc.sha_hash == "50976B6D16DAC4FE8D255CCA9EBA9D9E508C23EE824124DF988BA9B76A4DCC98"

    transaction = parse_transaction_doc(
        txdoc, "7F6oyFQywURCACWZZGtG97Girh9EL1kg2WBwftEZxDoJ", 310820, 1585901723, 0
    )

    assert transaction.currency == "testcurrency"
    # assert (
    #     transaction.sha_hash
    #     == "50976B6D16DAC4FE8D255CCA9EBA9D9E508C23EE824124DF988BA9B76A4DCC98"
    # )
    assert transaction.written_block == 310820
    assert transaction.blockstamp.number == 310818
    assert (
        transaction.blockstamp.sha_hash
        == "000002B26A7B623915E4955C633FB710D90758AA1D778B2D557518EA55B1F5FE"
    )
    assert transaction.timestamp == 1585901723
    assert (
        transaction.signatures[0]
        == "FPimg3/Xmb/qRIGP3ZjwV8yH/cbqb6AIgZ49lnwcQ2S9cIj1BDCdhKpDKfm3nFkLoGZUZXXkmGXSoTehtx5JDQ=="
    )
    assert transaction.amount == 2
    assert transaction.amount_base == 4
    assert transaction.issuers[0] == "EAHgNyYsxmS7YSfuQsEzEWwKnD2UyMzwVTuBnstMSk3e"
    assert transaction.receivers[0] == "7F6oyFQywURCACWZZGtG97Girh9EL1kg2WBwftEZxDoJ"
    assert transaction.comment == "pour le ssb de scuttlebutt dans duniterpy"
    assert transaction.txid == 0
