<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>AboutMoney</name>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/about_money_uic.py" line="56"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/about_money_uic.py" line="57"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/about_money_uic.py" line="58"/>
        <source>Rules</source>
        <translation>Règles</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/about_money_uic.py" line="59"/>
        <source>Money</source>
        <translation>Monnaie</translation>
    </message>
</context>
<context>
    <name>AboutPopup</name>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/about_uic.py" line="40"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/about_uic.py" line="41"/>
        <source>label</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AboutWot</name>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/about_wot_uic.py" line="33"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/about_wot_uic.py" line="34"/>
        <source>WoT</source>
        <translation>TdC</translation>
    </message>
</context>
<context>
    <name>BaseGraph</name>
    <message>
        <location filename="../../../src/sakia/data/graphs/base_graph.py" line="19"/>
        <source>(sentry)</source>
        <translation>(référent)</translation>
    </message>
</context>
<context>
    <name>CertificationController</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/controller.py" line="208"/>
        <source>{days} days</source>
        <translation>{days} jours</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/controller.py" line="212"/>
        <source>{hours}h {min}min</source>
        <translation>{hours}h {min}min</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/controller.py" line="113"/>
        <source>Certification</source>
        <translation>Certification</translation>
    </message>
</context>
<context>
    <name>CertificationView</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/view.py" line="36"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="25"/>
        <source>No more certifications</source>
        <translation>Plus assez de certifications</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="29"/>
        <source>Not a member</source>
        <translation>Non membre</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="33"/>
        <source>Please select an identity</source>
        <translation>Veuillez sélectionner une identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="37"/>
        <source>&amp;Ok (Not validated before {remaining})</source>
        <translation>&amp;Ok (Non validé avant {remaining})</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="43"/>
        <source>&amp;Process Certification</source>
        <translation>&amp;Procéder à la Certification</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="51"/>
        <source>Please enter correct password</source>
        <translation>Veuillez entrer un mot de passe correct</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="112"/>
        <source>Import identity document</source>
        <translation>Importer un document d&apos;identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="112"/>
        <source>Duniter documents (*.txt)</source>
        <translation>Documents Duniter (*.txt)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="127"/>
        <source>Identity document</source>
        <translation>Document d&apos;identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="176"/>
        <source>Certification</source>
        <translation>Certification</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="157"/>
        <source>Success sending certification</source>
        <translation>Succès de l&apos;envoi de la certification</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="203"/>
        <source>Certifications sent: {nb_certifications}/{stock}</source>
        <translation>Certifications envoyées: {nb_certifications}/{stock}</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="212"/>
        <source>{days} days</source>
        <translation>{days} jours</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/view.py" line="216"/>
        <source>{hours} hours and {min} min.</source>
        <translation>{hours} heures et {min} min.</translation>
    </message>
</context>
<context>
    <name>CertificationWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="139"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="140"/>
        <source>Select your identity</source>
        <translation>Sélectionnez votre identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="141"/>
        <source>Certifications stock</source>
        <translation>Stock de certifications</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="142"/>
        <source>Certify user</source>
        <translation>Certifier l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="143"/>
        <source>Import identity document</source>
        <translation>Importer un document d&apos;identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="144"/>
        <source>Process certification</source>
        <translation>Procéder à la certification</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="150"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="147"/>
        <source>Licence</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="148"/>
        <source>By going throught the process of creating a wallet, you accept the license above.</source>
        <translation>En procédant à la création d&apos;un portefeuille, vous acceptez la licence ci-dessus.</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="149"/>
        <source>I accept the above licence</source>
        <translation>J&apos;accepte la licence ci-dessus</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="151"/>
        <source>Secret Key / Password</source>
        <translation>Clé secrète / Mot de passe</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/certification/certification_uic.py" line="146"/>
        <source>Step 1. Check the key and user / Step 2. Accept the money licence / Step 3. Sign to confirm certification</source>
        <translation>Etape 1. Vérifiez la clé et l&apos;utilisateur / Etape 2. Acceptez la licence de la monnaie / Etape 3. Signez pour confirmer la certification</translation>
    </message>
</context>
<context>
    <name>CertifiersTableModel</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/table_model.py" line="127"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/table_model.py" line="128"/>
        <source>Pubkey</source>
        <translation>Clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/table_model.py" line="130"/>
        <source>Expiration</source>
        <translation>Expiration</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/table_model.py" line="129"/>
        <source>Publication</source>
        <translation>Publication</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/table_model.py" line="131"/>
        <source>available</source>
        <translation>disponible</translation>
    </message>
</context>
<context>
    <name>CongratulationPopup</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/congratulation_uic.py" line="51"/>
        <source>Congratulation</source>
        <translation>Félicitations</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/congratulation_uic.py" line="52"/>
        <source>label</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ConnectionConfigController</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="212"/>
        <source>Broadcasting identity...</source>
        <translation>Diffusion de votre identité...</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="579"/>
        <source>connecting...</source>
        <translation>connexion...</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="191"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="198"/>
        <source> (Optional)</source>
        <translation> (Optionnel)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="377"/>
        <source>Save a revocation document</source>
        <translation>Enregistrer le document de révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="377"/>
        <source>All text files (*.txt)</source>
        <translation>Tous les fichiers txt (*.txt)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="301"/>
        <source>Forbidden: pubkey is too short</source>
        <translation>Interdit : la clef publique est trop courte</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="308"/>
        <source>Forbidden: pubkey is too long</source>
        <translation>Interdit : la clef publique est trop longue</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="316"/>
        <source>Error: passwords are different</source>
        <translation>Erreur : mots de passe différents</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="324"/>
        <source>Error: salts are different</source>
        <translation>Erreur : les sels sont différents</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="356"/>
        <source>Forbidden: salt is too short</source>
        <translation>Interdit : le sel est trop court</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="364"/>
        <source>Forbidden: password is too short</source>
        <translation>Interdit : le mot de passe est trop court</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="396"/>
        <source>Revocation file</source>
        <translation>Fichier de révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/controller.py" line="103"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>ConnectionConfigView</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/view.py" line="139"/>
        <source>UID broadcast</source>
        <translation>Diffusion de l&apos;UID</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/view.py" line="129"/>
        <source>Identity broadcasted to the network</source>
        <translation>Identité diffusée sur le réseau</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/view.py" line="143"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/view.py" line="231"/>
        <source>{days} days, {hours}h  and {min}min</source>
        <translation>{days} jours, {hours}h  et {min}min</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/view.py" line="156"/>
        <source>New sakia account on {0} network</source>
        <translation>Nouveau compte sakia pour le réseau {0}</translation>
    </message>
</context>
<context>
    <name>ConnectionConfigurationDialog</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="260"/>
        <source>I accept the above licence</source>
        <translation>J&apos;accepte la licence ci-dessus</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="264"/>
        <source>Public key</source>
        <translation>Clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="266"/>
        <source>Secret key</source>
        <translation>Clé secrète</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="267"/>
        <source>Please repeat your secret key</source>
        <translation>Veuillez répéter votre clé secrète</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="268"/>
        <source>Your password</source>
        <translation>Votre mot de passe</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="269"/>
        <source>Please repeat your password</source>
        <translation>Veuillez répéter votre mot de passe</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="270"/>
        <source>Show public key</source>
        <translation>Voir la clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="271"/>
        <source>Scrypt parameters</source>
        <translation>Paramètres scrypt</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="272"/>
        <source>Simple</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="273"/>
        <source>Secure</source>
        <translation>Sécurisé</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="274"/>
        <source>Hardest</source>
        <translation>Difficile</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="275"/>
        <source>Extreme</source>
        <translation>Extrême</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="279"/>
        <source>Export revocation document to continue</source>
        <translation>Exporter le document de révocation pour continuer</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="237"/>
        <source>Add an account</source>
        <translation>Ajouter un compte</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="242"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Licence&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Licence&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="259"/>
        <source>By going throught the process of creating a wallet, you accept the licence above.</source>
        <translation>En procédant à la création d&apos;un portefeuille, vous acceptez la licence ci-dessus.</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="261"/>
        <source>Account parameters</source>
        <translation>Paramètres du compte</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="241"/>
        <source>Create a new member account</source>
        <translation>Créer un nouveau compte membre</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="240"/>
        <source>Add an existing member account</source>
        <translation>Ajouter un compte membre existant</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="239"/>
        <source>Add a wallet</source>
        <translation>Ajouter un portefeuille</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="238"/>
        <source>Add using a public key (quick)</source>
        <translation>Ajouter par une clé publique (rapide)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="262"/>
        <source>Identity name (UID)</source>
        <translation>Nom de l&apos;identité (UID)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="265"/>
        <source>Credentials</source>
        <translation>Identifiants</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="276"/>
        <source>N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="277"/>
        <source>r</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="278"/>
        <source>p</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/connection_cfg/connection_cfg_uic.py" line="243"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    This program is free software: you can redistribute it and/or modify&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    it under the terms of the GNU General Public License as published by&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    the Free Software Foundation, either version 3 of the License, or&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    (at your option) any later version.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    This program is distributed in the hope that it will be useful,&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    You should have received a copy of the GNU General Public License&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt;&quot;&gt;    along with this program.  If not, see &amp;lt;http://www.gnu.org/licenses/&amp;gt;. &lt;/span&gt;&lt;a name=&quot;TransNote1-rev&quot;&gt;&lt;/a&gt;&lt;a href=&quot;https://www.gnu.org/licenses/gpl-howto.fr.html#TransNote1&quot;&gt;&lt;span style=&quot; font-family:&apos;Hack&apos;; font-size:10pt; text-decoration: underline; color:#2980b9; vertical-align:super;&quot;&gt;1&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactDialog</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/contact_uic.py" line="109"/>
        <source>Contacts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/contact_uic.py" line="110"/>
        <source>Contacts list</source>
        <translation>Liste des contacts</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/contact_uic.py" line="111"/>
        <source>Delete selected contact</source>
        <translation>Supprimer le contact sélectionné</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/contact_uic.py" line="112"/>
        <source>Clear selection</source>
        <translation>Vider la sélection</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/contact_uic.py" line="113"/>
        <source>Contact informations</source>
        <translation>Information du contact</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/contact_uic.py" line="114"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/contact_uic.py" line="115"/>
        <source>Public key</source>
        <translation>Clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/contact_uic.py" line="116"/>
        <source>Add other informations</source>
        <translation>Ajouter d&apos;autres informations</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/contact_uic.py" line="117"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
</context>
<context>
    <name>ContactsTableModel</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/table_model.py" line="73"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/contact/table_model.py" line="73"/>
        <source>Public key</source>
        <translation>Clé publique</translation>
    </message>
</context>
<context>
    <name>ContextMenu</name>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="330"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="44"/>
        <source>Informations</source>
        <translation>Informations</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="54"/>
        <source>Certify identity</source>
        <translation>Certifier cette identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="63"/>
        <source>View in Web of Trust</source>
        <translation>Voir dans la Toile de Confiance</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="246"/>
        <source>Send money</source>
        <translation>Envoyer de la monnaie</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="223"/>
        <source>Copy pubkey to clipboard</source>
        <translation>Copier la clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="232"/>
        <source>Copy pubkey to clipboard (with CRC)</source>
        <translation>Copier la clé publique (avec CRC)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="98"/>
        <source>Copy self-certification document to clipboard</source>
        <translation>Copier le document d&apos;auto-certification</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="115"/>
        <source>Transfer</source>
        <translation>Transfert</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="119"/>
        <source>Send again</source>
        <translation>Renvoyer</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="128"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="166"/>
        <source>Copy raw transaction to clipboard</source>
        <translation>Copier la transaction (format brut)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="178"/>
        <source>Copy transaction block to clipboard</source>
        <translation>Copier le bloc de la transaction</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="210"/>
        <source>Send as source</source>
        <translation>Envoyer comme source</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/widgets/context_menu.py" line="197"/>
        <source>Dividend</source>
        <translation>Dividende</translation>
    </message>
</context>
<context>
    <name>HistoryTableModel</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/table_model.py" line="52"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/table_model.py" line="52"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/table_model.py" line="52"/>
        <source>Amount</source>
        <translation>Montant</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/table_model.py" line="52"/>
        <source>Public key</source>
        <translation>Clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/table_model.py" line="201"/>
        <source>Transactions missing from history</source>
        <translation>Transactions manquantes dans l&apos;historique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/table_model.py" line="499"/>
        <source>{0} / {1} confirmations</source>
        <translation>{0} / {1} confirmations</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/table_model.py" line="505"/>
        <source>Confirming... {0} %</source>
        <translation>Confirmation... {0} %</translation>
    </message>
</context>
<context>
    <name>HomescreenWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/homescreen/homescreen_uic.py" line="28"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
</context>
<context>
    <name>IdentitiesTableModel</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/table_model.py" line="151"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/table_model.py" line="152"/>
        <source>Pubkey</source>
        <translation>Clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/table_model.py" line="153"/>
        <source>Renewed</source>
        <translation>Renouvelée le</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/table_model.py" line="154"/>
        <source>Expiration</source>
        <translation>Expiration</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/table_model.py" line="156"/>
        <source>Publication Block</source>
        <translation>Bloc de publication</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/table_model.py" line="155"/>
        <source>Publication</source>
        <translation>Publication</translation>
    </message>
</context>
<context>
    <name>IdentitiesView</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/view.py" line="16"/>
        <source>Search direct certifications</source>
        <translation>Rechercher des certifications &quot;directes&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/view.py" line="19"/>
        <source>Research a pubkey, an uid...</source>
        <translation>Rechercher une clé publique, un uid...</translation>
    </message>
</context>
<context>
    <name>IdentitiesWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/identities_uic.py" line="46"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/identities_uic.py" line="47"/>
        <source>Research a pubkey, an uid...</source>
        <translation>Rechercher une clé publique, un uid...</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identities/identities_uic.py" line="48"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
</context>
<context>
    <name>IdentityController</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/controller.py" line="192"/>
        <source>Membership</source>
        <translation>Adhésion</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/controller.py" line="178"/>
        <source>Success sending Membership demand</source>
        <translation>Envoi de la demande d&apos;adhésion réussi</translation>
    </message>
</context>
<context>
    <name>IdentityModel</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/model.py" line="215"/>
        <source>Outdistanced</source>
        <translation>Hors distance</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/model.py" line="254"/>
        <source>In WoT range</source>
        <translation>Dans la TdC</translation>
    </message>
</context>
<context>
    <name>IdentityView</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="72"/>
        <source>Identity written in blockchain</source>
        <translation>Identité écrite en blockchain</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="82"/>
        <source>Identity not written in blockchain</source>
        <translation>Identité non écrite en blockchain</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="82"/>
        <source>Expires on: {0}</source>
        <translation>Expire le : {0}</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="93"/>
        <source>Member</source>
        <translation>Membre</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="93"/>
        <source>Not a member</source>
        <translation>Non membre</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="106"/>
        <source>Renew membership</source>
        <translation>Renouveler l&apos;adhésion</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="112"/>
        <source>Request membership</source>
        <translation>Demande d&apos;adhésion</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="116"/>
        <source>Identity registration ready</source>
        <translation>Enregistrement de l&apos;identité prêt</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="121"/>
        <source>{0} more certifications required</source>
        <translation>{0} certifications suppémentaires sont requises</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="128"/>
        <source>Expires in </source>
        <translation>Expire dans </translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="132"/>
        <source>{days} days</source>
        <translation>{days} jours</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="136"/>
        <source>{hours} hours and {min} min.</source>
        <translation>{hours} heures et {min} min.</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="140"/>
        <source>Expired or never published</source>
        <translation>Expirée ou jamais publiée</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="161"/>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="172"/>
        <source>Certs. received</source>
        <translation>Certs reçues</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="172"/>
        <source>Membership</source>
        <translation>Adhésion</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="221"/>
        <source>{:} day(s) {:} hour(s)</source>
        <translation>{:} jour(s) {:} heure(s)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="215"/>
        <source>{:} hour(s)</source>
        <translation>{:} heure(s)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="228"/>
        <source>Fundamental growth (c)</source>
        <translation>Croissance fondamentale (c)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="228"/>
        <source>Initial Universal Dividend UD(0) in</source>
        <translation>Dividende Universel Initial DU(0) en</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="228"/>
        <source>Time period between two UD</source>
        <translation>Durée entre deux DU</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="228"/>
        <source>Time period between two UD reevaluation</source>
        <translation>Durée entre deux réévaluations du DU</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="228"/>
        <source>Minimum delay between 2 certifications (in days)</source>
        <translation>Délai minimum entre 2 certifications (en jours)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="228"/>
        <source>Maximum validity time of a certification (in days)</source>
        <translation>Durée de validité d&apos;une certification (en jours)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="228"/>
        <source>Maximum time before a pending certification expire</source>
        <translation>Durée maximum avant qu&apos;une certification en attente n&apos;expire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/view.py" line="228"/>
        <source>Maximum validity time of a membership (in days)</source>
        <translation>Durée de validité d&apos;une adhésion (en jours)</translation>
    </message>
</context>
<context>
    <name>IdentityWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/identity_uic.py" line="109"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/identity_uic.py" line="110"/>
        <source>Certify an identity</source>
        <translation>Certifier une identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/identity_uic.py" line="111"/>
        <source>Membership status</source>
        <translation>Statut de l&apos;adhésion</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/identity/identity_uic.py" line="112"/>
        <source>Renew membership</source>
        <translation>Renouveler l&apos;adhésion</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="79"/>
        <source>Manage accounts</source>
        <translation>Gérer les comptes</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="80"/>
        <source>Configure trustable nodes</source>
        <translation>Configurer les noeuds de confiance</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="81"/>
        <source>A&amp;dd a contact</source>
        <translation>A&amp;jouter un contact</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="85"/>
        <source>Send a message</source>
        <translation>Envoyer un message</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="86"/>
        <source>Send money</source>
        <translation>Envoyer de la monnaie</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="87"/>
        <source>Remove contact</source>
        <translation>Supprimer le contact</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="88"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="89"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="90"/>
        <source>Account</source>
        <translation>Compte</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="91"/>
        <source>&amp;Transfer money</source>
        <translation>&amp;Transferer de la monnaie</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="92"/>
        <source>&amp;Configure</source>
        <translation>&amp;Configurer</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="93"/>
        <source>&amp;Import</source>
        <translation>&amp;Importer</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="94"/>
        <source>&amp;Export</source>
        <translation>&amp;Exporter</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="95"/>
        <source>C&amp;ertification</source>
        <translation>C&amp;ertification</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="96"/>
        <source>&amp;Set as default</source>
        <translation>&amp;Par défaut</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="97"/>
        <source>A&amp;bout</source>
        <translation>A&amp;propos</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="98"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Préférences</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="99"/>
        <source>&amp;Add account</source>
        <translation>&amp;Ajouter un compte</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="100"/>
        <source>&amp;Manage local node</source>
        <translation>&amp;Gérer le noeud local</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/mainwindow_uic.py" line="101"/>
        <source>&amp;Revoke an identity</source>
        <translation>&amp;Révoquer une identité</translation>
    </message>
</context>
<context>
    <name>MainWindowController</name>
    <message>
        <location filename="../../../src/sakia/gui/main_window/controller.py" line="111"/>
        <source>Please get the latest release {version}</source>
        <translation>Veuillez télécharger la dernière version {version}</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/controller.py" line="132"/>
        <source>sakia {0} - {1}</source>
        <translation>sakia {0} - {1}</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/navigation_uic.py" line="48"/>
        <source>Frame</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NavigationController</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="180"/>
        <source>Publish UID</source>
        <translation>Publier votre UID</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="206"/>
        <source>Leave the currency</source>
        <translation>Quitter la monnaie</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="300"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="287"/>
        <source>Success publishing your UID</source>
        <translation>Publication de votre UID réussie</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="308"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="350"/>
        <source>Revoke</source>
        <translation>Révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="336"/>
        <source>Success sending Revoke demand</source>
        <translation>Demande de révocation réussie</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="438"/>
        <source>All text files (*.txt)</source>
        <translation>Tous les fichiers txt (*.txt)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="156"/>
        <source>View in Web of Trust</source>
        <translation>Voir dans la Toile de Confiance</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="193"/>
        <source>Export identity document</source>
        <translation>Exporter le document d&apos;identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="438"/>
        <source>Save an identity document</source>
        <translation>Sauvegarder un document d&apos;identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="459"/>
        <source>Identity file</source>
        <translation>Fichier Identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="246"/>
        <source>Remove the Sakia account</source>
        <translation>Supprimer le compte Sakia</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="358"/>
        <source>Removing the Sakia account</source>
        <translation>Suppression du compte Sakia</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="167"/>
        <source>Save revocation document</source>
        <translation>Sauvegarder le document de revocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="386"/>
        <source>Save a revocation document</source>
        <translation>Sauvegarder un document de révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="407"/>
        <source>Revocation file</source>
        <translation>Fichier de révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="220"/>
        <source>Copy pubkey to clipboard</source>
        <translation>Copier la clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/controller.py" line="233"/>
        <source>Copy pubkey to clipboard (with CRC)</source>
        <translation>Copier la clé publique (avec CRC)</translation>
    </message>
</context>
<context>
    <name>NavigationModel</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/model.py" line="42"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/model.py" line="106"/>
        <source>Transfers</source>
        <translation>Transferts</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/model.py" line="50"/>
        <source>Identities</source>
        <translation>Identités</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/model.py" line="60"/>
        <source>Web of Trust</source>
        <translation>Toile de Confiance</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/model.py" line="70"/>
        <source>Personal accounts</source>
        <translation>Comptes personnels</translation>
    </message>
</context>
<context>
    <name>NetworkController</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/controller.py" line="57"/>
        <source>Open in browser</source>
        <translation>Ouvrir dans le navigateur</translation>
    </message>
</context>
<context>
    <name>NetworkTableModel</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="192"/>
        <source>Online</source>
        <translation>Connecté</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="193"/>
        <source>Offline</source>
        <translation>Déconnecté</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="194"/>
        <source>Unsynchronized</source>
        <translation>Désynchronisé</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="90"/>
        <source>yes</source>
        <translation>oui</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="91"/>
        <source>no</source>
        <translation>non</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="92"/>
        <source>offline</source>
        <translation>déconnecté</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="148"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="149"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="150"/>
        <source>API</source>
        <translation>API</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="151"/>
        <source>Block</source>
        <translation>Bloc</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="152"/>
        <source>Hash</source>
        <translation>Hash</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="153"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="154"/>
        <source>Member</source>
        <translation>Membre</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="155"/>
        <source>Pubkey</source>
        <translation>Clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="156"/>
        <source>Software</source>
        <translation>Logiciel</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/table_model.py" line="157"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
</context>
<context>
    <name>NetworkWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/network/network_uic.py" line="52"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PasswordInputController</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/password_input/controller.py" line="83"/>
        <source>Non printable characters in password</source>
        <translation>Caractères invisibles présents dans le mot de passe</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/password_input/controller.py" line="75"/>
        <source>Non printable characters in secret key</source>
        <translation>Caractères invisibles présents dans la clef secrète</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/password_input/controller.py" line="52"/>
        <source>Please enter your password</source>
        <translation>Veuillez entrer votre mot de passe</translation>
    </message>
</context>
<context>
    <name>PasswordInputView</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/password_input/view.py" line="37"/>
        <source>Password is valid</source>
        <translation>Mot de passe valide</translation>
    </message>
</context>
<context>
    <name>PasswordInputWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/password_input/password_input_uic.py" line="37"/>
        <source>Please enter your password</source>
        <translation>Veuillez entrer votre mot de passe</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/password_input/password_input_uic.py" line="36"/>
        <source>Please enter your secret key</source>
        <translation>Veuillez entrer votre clé secrète</translation>
    </message>
</context>
<context>
    <name>PercentOfAverage</name>
    <message>
        <location filename="../../../src/sakia/money/percent_of_average.py" line="12"/>
        <source>PoA</source>
        <translation>PàM</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/percent_of_average.py" line="11"/>
        <source>{0} {1}{2}</source>
        <translation>{0} {1}{2}</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/percent_of_average.py" line="13"/>
        <source>PoA = (Q / ( M(t-1) / N)) / 100
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;PoA&lt;/td&gt;&lt;td&gt;Percent of Average value&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;Q&lt;/td&gt;&lt;td&gt;Quantitative value&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;M&lt;/td&gt;&lt;td&gt;Monetary mass&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;N&lt;/td&gt;&lt;td&gt;Members count&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;</source>
        <translation>PàM = (Q / ( M(t-1) / N)) / 100
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;PàM&lt;/td&gt;&lt;td&gt;Valeur du Pourcentage à la Moyenne&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;Q&lt;/td&gt;&lt;td&gt;Valeur quantitative&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;M&lt;/td&gt;&lt;td&gt;Masse monétaire&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;N&lt;/td&gt;&lt;td&gt;Nombre de membres&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/percent_of_average.py" line="24"/>
        <source>Another relative referential of the money.&lt;br /&gt;
                                          Percent of Average value PoA is calculated by dividing the quantitative value Q by the average&lt;br /&gt;
                                           then multiply by one hundred.&lt;br /&gt;
                                          This referential is relative and can be used to display prices and accounts, when UD growth is too slow.&lt;br /&gt;
                                          No money creation or destruction is apparent here and every account tend to&lt;br /&gt;
                                           the 100%.
                                          </source>
        <translation>Un autre référentiel relatif de la monnaie.&lt;br /&gt;
                                          Le Pourcentage à la Moyenne PàM est calculé en divisant la valeur quantitative Q par la moyenne&lt;br /&gt;
                                           puis en multipliant par 100.&lt;br /&gt;
                                          Ce référentiel est relatif et peut être utilisé pour afficher les prix et les comptes, quand la croissance du DU est trop lente.&lt;br /&gt;
                                          Aucune création ou destruction de monnaie n&apos;est apparente ici et les comptes convergent tous&lt;br /&gt;
                                           vers 100%.
                                          </translation>
    </message>
</context>
<context>
    <name>PluginDialog</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/plugins_manager_uic.py" line="52"/>
        <source>Plugins manager</source>
        <translation>Gestionnaire de plugins</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/plugins_manager_uic.py" line="53"/>
        <source>Installed plugins list</source>
        <translation>Liste des plugins installés</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/plugins_manager_uic.py" line="54"/>
        <source>Install a new plugin</source>
        <translation>Installer un nouveau plugin</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/plugins_manager_uic.py" line="55"/>
        <source>Uninstall selected plugin</source>
        <translation>Désinstaller le plugin sélectionné</translation>
    </message>
</context>
<context>
    <name>PluginsManagerController</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/controller.py" line="60"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/controller.py" line="60"/>
        <source>Sakia module (*.zip)</source>
        <translation>module Sakia (*.zip)</translation>
    </message>
</context>
<context>
    <name>PluginsManagerView</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/view.py" line="43"/>
        <source>Plugin import</source>
        <translation>Import de plugin</translation>
    </message>
</context>
<context>
    <name>PluginsTableModel</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/table_model.py" line="67"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/table_model.py" line="67"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/table_model.py" line="67"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/plugins_manager/table_model.py" line="67"/>
        <source>Imported</source>
        <translation>Importé</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="214"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="215"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="216"/>
        <source>Display</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="217"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="218"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;General settings&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Préférences générales&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="219"/>
        <source>Default &amp;referential</source>
        <translation>&amp;référentiel par défaut</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="220"/>
        <source>Enable expert mode</source>
        <translation>Activer le mode expert</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="221"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Display settings&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Préférences affichage&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="222"/>
        <source>Digits after commas </source>
        <translation>Chiffres après la virgule </translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="223"/>
        <source>Language</source>
        <translation>Langage</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="224"/>
        <source>Maximize Window at Startup</source>
        <translation>Agrandir la Fenêtre au Démarrage</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="225"/>
        <source>Enable notifications</source>
        <translation>Activer les notifications</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="226"/>
        <source>Dark Theme compatibility</source>
        <translation>Compatibilité thème sombre</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="227"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Network settings&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Préférences réseau&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="228"/>
        <source>Use a http proxy server</source>
        <translation>Utiliser un serveur proxy http</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="229"/>
        <source>Proxy server address</source>
        <translation>Adresse serveur proxy</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="230"/>
        <source>:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="231"/>
        <source>Proxy username</source>
        <translation>Nom d&apos;utilisateur du proxy</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/preferences_uic.py" line="232"/>
        <source>Proxy password</source>
        <translation>Mot de passe du proxy</translation>
    </message>
</context>
<context>
    <name>Quantitative</name>
    <message>
        <location filename="../../../src/sakia/money/quantitative.py" line="8"/>
        <source>Units</source>
        <translation>Unités</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/quantitative.py" line="9"/>
        <source>{0} {1}{2}</source>
        <translation>{0} {1}{2}</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/quantitative.py" line="20"/>
        <source>Base referential of the money. Units values are used here.</source>
        <translation>Référentiel de base de la monnaie. Les unités sont utilisées ici.</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/quantitative.py" line="10"/>
        <source>units</source>
        <translation>unités</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/quantitative.py" line="11"/>
        <source>Q = Q
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;Q&lt;/td&gt;&lt;td&gt;Quantitative value&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;
                                      </source>
        <translation>Q = Q
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;Q&lt;/td&gt;&lt;td&gt;Valeur quantitative&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;
                                      </translation>
    </message>
</context>
<context>
    <name>QuantitativeZSum</name>
    <message>
        <location filename="../../../src/sakia/money/quant_zerosum.py" line="9"/>
        <source>Quant Z-sum</source>
        <translation>Quant. som. 0</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/quant_zerosum.py" line="10"/>
        <source>{0}{1}{2}</source>
        <translation>{0}{1}{2}</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/quant_zerosum.py" line="11"/>
        <source>Q0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/quant_zerosum.py" line="24"/>
        <source>Quantitative at zero sum is used to display the difference between&lt;br /&gt;
                                            the quantitative value and the average quantitative value.&lt;br /&gt;
                                            If it is positive, the value is above the average value, and if it is negative,&lt;br /&gt;
                                            the value is under the average value.&lt;br /&gt;
                                           </source>
        <translation>Le quantitatif à somme nulle est utilisé pour afficher la différence entre&lt;br /&gt;
                                            la valeur quantitative et la valeur moyenne.&lt;br /&gt;
                                            Si c&apos;est positif, la valeur est au-dessus de la moyenne, et si c&apos;est négatif,&lt;br /&gt;
                                            la valeur est en-dessous de la moyenne.&lt;br /&gt;
                                           </translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/quant_zerosum.py" line="12"/>
        <source>Q0 = Q - ( M(t) / N(t) )
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;Q0&lt;/td&gt;&lt;td&gt;Quantitative value at zero sum&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;Q&lt;/td&gt;&lt;td&gt;Quantitative value&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;M&lt;/td&gt;&lt;td&gt;Monetary mass&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;N&lt;/td&gt;&lt;td&gt;Members count&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;t&lt;/td&gt;&lt;td&gt;Last UD time&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;</source>
        <translation>Q0 = Q - ( M(t) / N(t) )
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;Q0&lt;/td&gt;&lt;td&gt;Valeur quantitative à somme nulle&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;Q&lt;/td&gt;&lt;td&gt;Valeur quantitative&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;M&lt;/td&gt;&lt;td&gt;Masse monétaire&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;N&lt;/td&gt;&lt;td&gt;Nombre de membres&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;t&lt;/td&gt;&lt;td&gt;Temps du dernier DU&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;</translation>
    </message>
</context>
<context>
    <name>Relative</name>
    <message>
        <location filename="../../../src/sakia/money/relative.py" line="11"/>
        <source>UD</source>
        <translation>DU</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/relative.py" line="10"/>
        <source>{0} {1}{2}</source>
        <translation>{0} {1}{2}</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/relative.py" line="12"/>
        <source>R = Q / UD(t)
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;R&lt;/td&gt;&lt;td&gt;Relative value&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;Q&lt;/td&gt;&lt;td&gt;Quantitative value&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;UD&lt;/td&gt;&lt;td&gt;Universal Dividend&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;t&lt;/td&gt;&lt;td&gt;Last UD time&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;</source>
        <translation>R = Q / UD(t)
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;R&lt;/td&gt;&lt;td&gt;Valeur relative&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;Q&lt;/td&gt;&lt;td&gt;Valeur quantitative&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;UD&lt;/td&gt;&lt;td&gt;Dividende Universel&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;t&lt;/td&gt;&lt;td&gt;Temps du dernier DU&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/relative.py" line="23"/>
        <source>Relative referential of the money.&lt;br /&gt;
                                          Relative value R is calculated by dividing the quantitative value Q by the last&lt;br /&gt;
                                           Universal Dividend UD.&lt;br /&gt;
                                          This referential is the most practical one to display prices and accounts.&lt;br /&gt;
                                          No money creation or destruction is apparent here and every account tend to&lt;br /&gt;
                                           the average.
                                          </source>
        <translation>Référentiel relatif de la monnaie.&lt;br /&gt;
                                          La valeur relative R est calculée en divisant la valeur quantitative par la valeur du dernier&lt;br /&gt;
                                           Dividende Universel DU.&lt;br /&gt;
                                          Ce référentiel est le plus pratique pour afficher les prix et les comptes.&lt;br /&gt;
                                          Aucune création ou destruction de monnaie n&apos;est apparente ici et tous les comptes convergent vers&lt;br /&gt;
                                           la moyenne.
                                          </translation>
    </message>
</context>
<context>
    <name>RelativeZSum</name>
    <message>
        <location filename="../../../src/sakia/money/relative_zerosum.py" line="9"/>
        <source>Relat Z-sum</source>
        <translation>Rel. som. 0</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/relative_zerosum.py" line="10"/>
        <source>{0} {1}{2}</source>
        <translation>{0} {1}{2}</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/relative_zerosum.py" line="11"/>
        <source>R0 UD</source>
        <translation>R0 DU</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/relative_zerosum.py" line="24"/>
        <source>Relative at zero sum is used to display the difference between&lt;br /&gt;
                                            the relative value and the average relative value.&lt;br /&gt;
                                            If it is positive, the value is above the average value, and if it is negative,&lt;br /&gt;
                                            the value is under the average value.&lt;br /&gt;
                                           </source>
        <translation>Le référentiel relatif à somme nulle est utilisé pour afficher la différence entre&lt;br /&gt;
                                            la valeur relative et la valeur relative moyenne.&lt;br /&gt;
                                            Si c&apos;est positif, la valeur est au-dessus de la moyenne, et si c&apos;est négatif,&lt;br /&gt;
                                            la valeur est en-dessous de la moyenne.&lt;br /&gt;
                                           </translation>
    </message>
    <message>
        <location filename="../../../src/sakia/money/relative_zerosum.py" line="12"/>
        <source>R0 = (Q / UD(t)) - (( M(t) / N(t) ) / UD(t))
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;R0&lt;/td&gt;&lt;td&gt;Relative value at zero sum&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;R&lt;/td&gt;&lt;td&gt;Relative value&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;M&lt;/td&gt;&lt;td&gt;Monetary mass&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;N&lt;/td&gt;&lt;td&gt;Members count&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;t&lt;/td&gt;&lt;td&gt;Last UD time&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;</source>
        <translation>R0 = (Q / UD(t)) - (( M(t) / N(t) ) / UD(t))
                                        &lt;br &gt;
                                        &lt;table&gt;
                                        &lt;tr&gt;&lt;td&gt;R0&lt;/td&gt;&lt;td&gt;Valeur relative à somme nulle&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;R&lt;/td&gt;&lt;td&gt;Valeur relative&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;M&lt;/td&gt;&lt;td&gt;Masse monétaire&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;N&lt;/td&gt;&lt;td&gt;Nombre de membres&lt;/td&gt;&lt;/tr&gt;
                                        &lt;tr&gt;&lt;td&gt;t&lt;/td&gt;&lt;td&gt;Temps du dernier DU&lt;/td&gt;&lt;/tr&gt;
                                        &lt;/table&gt;</translation>
    </message>
</context>
<context>
    <name>RevocationDialog</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="142"/>
        <source>Revoke an identity</source>
        <translation>Révoquer une identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="143"/>
        <source>&lt;h2&gt;Select a revocation document&lt;/h1&gt;</source>
        <translation>&lt;h2&gt;Sélectionnez un document de révocation&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="144"/>
        <source>Load from file</source>
        <translation>Charger depuis le fichier</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="145"/>
        <source>Revocation document</source>
        <translation>Document de révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="146"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;Select publication destination&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;Sélectionnez la destination de publication&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="147"/>
        <source>To a co&amp;mmunity</source>
        <translation>Vers la co&amp;mmunauté</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="148"/>
        <source>&amp;To an address</source>
        <translation>&amp;Vers une adresse</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="149"/>
        <source>SSL/TLS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="150"/>
        <source>Revocation information</source>
        <translation>Information de révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/revocation_uic.py" line="151"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>RevocationView</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/view.py" line="125"/>
        <source>Load a revocation file</source>
        <translation>Charger un fichier de révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/view.py" line="125"/>
        <source>All text files (*.txt)</source>
        <translation>Tous les fichiers txt (*.txt)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/view.py" line="135"/>
        <source>Error loading document</source>
        <translation>Erreur au chargement du document</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/view.py" line="135"/>
        <source>Loaded document is not a revocation document</source>
        <translation>Le document chargé n&apos;est pas un document de révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/view.py" line="145"/>
        <source>Error broadcasting document</source>
        <translation>Erreur à la diffusion du document</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/view.py" line="172"/>
        <source>Revocation</source>
        <translation>Révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/view.py" line="192"/>
        <source>Revocation broadcast</source>
        <translation>Diffusion de la révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/revocation/view.py" line="192"/>
        <source>The document was successfully broadcasted.</source>
        <translation>Le document a été diffusé avec succès.</translation>
    </message>
</context>
<context>
    <name>SakiaToolbar</name>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/toolbar_uic.py" line="79"/>
        <source>Frame</source>
        <translation>Cadre</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/toolbar_uic.py" line="80"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/toolbar_uic.py" line="81"/>
        <source>Search an identity</source>
        <translation>Rechercher une identité</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/toolbar_uic.py" line="82"/>
        <source>Explore</source>
        <translation>Explorer</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/toolbar_uic.py" line="83"/>
        <source>Contacts</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchUserView</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/search_user/view.py" line="63"/>
        <source>Looking for {0}...</source>
        <translation>Recherche de {0}...</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/search_user/view.py" line="20"/>
        <source>Research a pubkey, an uid...</source>
        <translation>Rechercher une clé publique, un uid...</translation>
    </message>
</context>
<context>
    <name>SearchUserWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/search_user/search_user_uic.py" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/search_user/search_user_uic.py" line="36"/>
        <source>Center the view on me</source>
        <translation>Centrer la vue sur moi</translation>
    </message>
</context>
<context>
    <name>SourcesServices</name>
    <message>
        <location filename="../../../src/sakia/services/sources.py" line="11"/>
        <source>missing secret key for public key</source>
        <translation>clé secrète manquante pour clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/services/sources.py" line="14"/>
        <source>missing password for hash</source>
        <translation>mot de passe manquant pour hash</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/services/sources.py" line="17"/>
        <source>locked by a delay until</source>
        <translation>verrouillé par un délai jusqu&apos;au</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/services/sources.py" line="20"/>
        <source>locked until</source>
        <translation>verrouillé jusqu&apos;au</translation>
    </message>
</context>
<context>
    <name>StartupDialog</name>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/startup_uic.py" line="54"/>
        <source>Sakia</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/startup_uic.py" line="55"/>
        <source>Connecting to the network
please wait...</source>
        <translation>Connexion au réseau en cours
veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/dialogs/startup_uic.py" line="57"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>StatusBarController</name>
    <message>
        <location filename="../../../src/sakia/gui/main_window/status_bar/controller.py" line="76"/>
        <source>Blockchain sync: {0} BAT ({1})</source>
        <translation>Synchro blockchain : {0} BAT ({1})</translation>
    </message>
</context>
<context>
    <name>Toast</name>
    <message>
        <location filename="../../../src/sakia/gui/widgets/toast_uic.py" line="39"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ToolbarView</name>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="27"/>
        <source>Publish a revocation document</source>
        <translation>Publier un document de révocation</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="35"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="53"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="65"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="122"/>
        <source>Membership</source>
        <translation>Adhésion</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="58"/>
        <source>Plugins manager</source>
        <translation>Gestionnaire de plugins</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="70"/>
        <source>About Money</source>
        <translation>A propos de la monnaie</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="75"/>
        <source>About Referentials</source>
        <translation>A propos des référentiels</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="80"/>
        <source>About Web of Trust</source>
        <translation>A propos de la Toile de Confiance</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="85"/>
        <source>About Sakia</source>
        <translation>A propos de Sakia</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="147"/>
        <source>Minimum delay between 2 certifications (days)</source>
        <translation>Délai minimum entre 2 certifications (jours)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="202"/>
        <source>Web of Trust rules</source>
        <translation>Règles de la Toile de Confiance</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="214"/>
        <source>Money rules</source>
        <translation>Règles de la monnaie</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="229"/>
        <source>Referentials</source>
        <translation>Référentiels</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="252"/>
        <source>Universal Dividend UD(t) in</source>
        <translation>Dividende Universel DU(t) en</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="252"/>
        <source>Monetary Mass M(t) in</source>
        <translation>Masse Monétaire M(t) en</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="252"/>
        <source>Members N(t)</source>
        <translation>Membres N(t)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="252"/>
        <source>Monetary Mass per member M(t)/N(t) in</source>
        <translation>Masse Monétaire par membre M(t)/N(t) en</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="252"/>
        <source>day</source>
        <translation>jour</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="252"/>
        <source>Actual growth c = UD(t)/[M(t)/N(t)]</source>
        <translation>Croissance réelle c = DU(t)/[M(t)/N(t)]</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="252"/>
        <source>Last UD date and time (t)</source>
        <translation>Date et heure du dernier DU (t)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="252"/>
        <source>Next UD date and time (t+1)</source>
        <translation>Date et heure du prochain DU (t+1)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="252"/>
        <source>Next UD reevaluation (t+1)</source>
        <translation>Prochaine réévaluation du DU (t+1)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="297"/>
        <source>{:2.2%} / {:} days</source>
        <translation>{:2.2%} / {:} jours</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="297"/>
        <source>UDÄ(t) = UDÄ(t-1) + cÂ²*M(t-1)/N(t)</source>
        <translation>DUÄ(t) = DUÄ(t-1) + cÂ²*M(t-1)/N(t)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="297"/>
        <source>Universal Dividend (formula)</source>
        <translation>Dividende Universel (formule)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="334"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="334"/>
        <source>Units</source>
        <translation>Unités</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="334"/>
        <source>Formula</source>
        <translation>Formule</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="334"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="364"/>
        <source>{:} day(s) {:} hour(s)</source>
        <translation>{:} jour(s) {:} heure(s)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="358"/>
        <source>{:} hour(s)</source>
        <translation>{:} heure(s)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="383"/>
        <source>Fundamental growth (c)</source>
        <translation>Croissance fondamentale (c)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="383"/>
        <source>Initial Universal Dividend UD(0) in</source>
        <translation>Dividende Universel Initial DU(0) en</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="383"/>
        <source>Time period between two UD</source>
        <translation>Durée entre deux DU</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="383"/>
        <source>Time period between two UD reevaluation</source>
        <translation>Durée entre deux réévaluations du DU</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="383"/>
        <source>Number of blocks used for calculating median time</source>
        <translation>Nombre de blocs utilisés pour calculer le temps median</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="40"/>
        <source>Add an Sakia account</source>
        <translation>Ajouter un compte Sakia</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="125"/>
        <source>Select an account</source>
        <translation>Sélectionner un compte</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="147"/>
        <source>Maximum validity time of a certification (days)</source>
        <translation>Durée de validité maximum d&apos;une certification (jours)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="147"/>
        <source>Maximum validity time of a membership (days)</source>
        <translation>Durée de validité maximum d&apos;une adhésion (jours)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/main_window/toolbar/view.py" line="90"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>TransferController</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/controller.py" line="173"/>
        <source>Transfer</source>
        <translation>Transfert</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/controller.py" line="390"/>
        <source>Check is successful!</source>
        <translation>Vérification réussie !</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/controller.py" line="397"/>
        <source>&lt;p&gt;&lt;b&gt;Condition&lt;/b&gt;&lt;/p&gt;{}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/controller.py" line="400"/>
        <source>&lt;p&gt;&lt;b&gt;Errors&lt;/b&gt;&lt;p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Erreurs&lt;/b&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/controller.py" line="426"/>
        <source>Check source condition</source>
        <translation>Verification condition source</translation>
    </message>
</context>
<context>
    <name>TransferMoneyWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="276"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="281"/>
        <source>Transfer money to</source>
        <translation>Virement vers</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="282"/>
        <source>&amp;Recipient public key</source>
        <translation>&amp;Clé publique du destinataire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="283"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="284"/>
        <source>Search &amp;user</source>
        <translation>Rechercher &amp;utilisateur</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="285"/>
        <source>Local ke&amp;y</source>
        <translation>C&amp;lé locale</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="286"/>
        <source>Con&amp;tact</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="287"/>
        <source>Available money: </source>
        <translation>Monnaie disponible : </translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="288"/>
        <source>Amount</source>
        <translation>Montant</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="295"/>
        <source>Secret Key / Password</source>
        <translation>Clé secrète / Mot de passe</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="277"/>
        <source>Select account</source>
        <translation>Sélectionnez un compte</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="291"/>
        <source>Message</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="292"/>
        <source>Spend condition</source>
        <translation>Condition de dépense</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="293"/>
        <source>Receiver signature</source>
        <translation>Signature destinataire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="294"/>
        <source>Receiver signature or (sender after one week)</source>
        <translation>Signature destinataire ou (émetteur après une semaine)</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="278"/>
        <source>Source</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="279"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="280"/>
        <source>Check</source>
        <translation>Vérifier</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/transfer_uic.py" line="290"/>
        <source>Units</source>
        <translation>Unités</translation>
    </message>
</context>
<context>
    <name>TransferView</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/view.py" line="30"/>
        <source>No amount. Please give the transfer amount</source>
        <translation>Aucun montant. Veuillez donner un montant de transfert</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/view.py" line="37"/>
        <source>Please enter correct password</source>
        <translation>Veuillez entrer un mot de passe correct</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/view.py" line="41"/>
        <source>Please enter a receiver</source>
        <translation>Veuillez entrer un destinataire</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/view.py" line="45"/>
        <source>Incorrect receiver address or pubkey</source>
        <translation>Adresse ou clé publique du destinataire incorrecte</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/view.py" line="229"/>
        <source>Transfer</source>
        <translation>Transfert</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/view.py" line="214"/>
        <source>Success sending money to {0}</source>
        <translation>Envoi de monnaie à {0} réussi</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/transfer/view.py" line="49"/>
        <source>Source locked</source>
        <translation>Source verrouillée</translation>
    </message>
</context>
<context>
    <name>TxHistoryController</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/controller.py" line="95"/>
        <source>Received {amount} from {number} transfers</source>
        <translation>Vous avez reçu {amount} via {number} transferts</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/controller.py" line="99"/>
        <source>New transactions received</source>
        <translation>Nouvelles transactions reçues</translation>
    </message>
</context>
<context>
    <name>TxHistoryModel</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/model.py" line="137"/>
        <source>Loading...</source>
        <translation>Chargement...</translation>
    </message>
</context>
<context>
    <name>TxHistoryView</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/view.py" line="63"/>
        <source> / {:} pages</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TxHistoryWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/txhistory_uic.py" line="115"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/txhistory_uic.py" line="116"/>
        <source>Balance</source>
        <translation>Solde</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/txhistory_uic.py" line="117"/>
        <source>loading...</source>
        <translation>chargement...</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/txhistory_uic.py" line="118"/>
        <source>Send money</source>
        <translation>Envoyer de la monnaie</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/txhistory_uic.py" line="120"/>
        <source>dd/MM/yyyy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/navigation/txhistory/txhistory_uic.py" line="121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#0000ff;&quot;&gt;■&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;to send &lt;/span&gt;&lt;span style=&quot; color:#ffb000;&quot;&gt;■&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;pending &lt;/span&gt;&lt;span style=&quot; color:#808080;&quot;&gt;■&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;refused &lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;■&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;not confirmed &lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;■&lt;/span&gt;validated &lt;span style=&quot; color:#000000;&quot;&gt;■ &lt;/span&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;to unlock&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#0000ff;&quot;&gt;■&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;à envoyer &lt;/span&gt;&lt;span style=&quot; color:#ffb000;&quot;&gt;■&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;en attente &lt;/span&gt;&lt;span style=&quot; color:#808080;&quot;&gt;■&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;refusée &lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;■&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;non confirmée &lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;■&lt;/span&gt;validée &lt;span style=&quot; color:#000000;&quot;&gt;■ &lt;/span&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;à déverrouiller&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>UserInformationView</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/view.py" line="72"/>
        <source>Public key</source>
        <translation>Clé publique</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/view.py" line="72"/>
        <source>UID Published on</source>
        <translation>UID Publié le</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/view.py" line="72"/>
        <source>Join date</source>
        <translation>Date d&apos;inscription</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/view.py" line="72"/>
        <source>Expires in</source>
        <translation>Expire dans</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/view.py" line="72"/>
        <source>Certs. received</source>
        <translation>Certs reçues</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/view.py" line="96"/>
        <source>Member</source>
        <translation>Membre</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/view.py" line="101"/>
        <source>#FF0000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/view.py" line="96"/>
        <source>Not a member</source>
        <translation>Non membre</translation>
    </message>
</context>
<context>
    <name>UserInformationWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/user_information_uic.py" line="76"/>
        <source>Member informations</source>
        <translation>Informations membre</translation>
    </message>
    <message>
        <location filename="../../../src/sakia/gui/sub/user_information/user_information_uic.py" line="77"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
</context>
<context>
    <name>WotWidget</name>
    <message>
        <location filename="../../../src/sakia/gui/navigation/graphs/wot/wot_tab_uic.py" line="27"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
</TS>
