#!/bin/bash
#./linuxdeploy-x86_64.AppImage --appdir AppDir -i sakia.png -d sakia.desktop --plugin python --output appimage --custom-apprun sakia.sh
# workaround to run inside a docker container
./linuxdeploy-x86_64.AppImage --appimage-extract
mv squashfs-root linuxdeploy-x86_64.AppDir
./linuxdeploy-x86_64.AppDir/AppRun --appdir AppDir  -i sakia.png -d sakia.desktop --plugin python --output appimage --custom-apprun sakia.sh
