#!/bin/bash

# Export APPRUN if running from an extracted image
self="$(readlink -f -- $0)"
here="${self%/*}"
APPDIR="${APPDIR:-${here}}"

# Call the entry point
${APPDIR}/usr/bin/python3.6 -s ${APPDIR}/usr/python/lib/python3.6/site-packages/sakia/main.py "$@"
